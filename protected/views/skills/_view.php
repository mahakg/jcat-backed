<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('skill_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->skill_id), array('view', 'id'=>$data->skill_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('skill_name')); ?>:</b>
	<?php echo CHtml::encode($data->skill_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('skill_desc')); ?>:</b>
	<?php echo CHtml::encode($data->skill_desc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('skill_img')); ?>:</b>
	<?php echo CHtml::encode($data->skill_img); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('skill_added')); ?>:</b>
	<?php echo CHtml::encode($data->skill_added); ?>
	<br />


</div>