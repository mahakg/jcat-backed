<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'skills-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'skill_name'); ?>
		<?php echo $form->textField($model,'skill_name',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'skill_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'skill_desc'); ?>
		<?php echo $form->textField($model,'skill_desc',array('size'=>60,'maxlength'=>500)); ?>
		<?php echo $form->error($model,'skill_desc'); ?>
	</div>

	<div class="row">
	<label>Upload Image</label>
	<div class="field_wrapper">
	<div class="files">
  	<input type="file" name="Skills[skill_img]" value="" id="content_files_1" class="MultiFile-applied">
  	</div>
	</div>	
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'skill_added'); ?>
		<?php echo $form->textField($model,'skill_added'); ?>
		<?php echo $form->error($model,'skill_added'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
