<?php

/**
 * This is the model class for table "jcat_skills".
 *
 * The followings are the available columns in table 'jcat_skills':
 * @property string $skill_id
 * @property string $skill_name
 * @property string $skill_desc
 * @property string $skill_img
 * @property string $skill_added
 *
 * The followings are the available model relations:
 * @property JcatQuestions[] $jcatQuestions
 */
class Skills extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Skills the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'jcat_skills';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('skill_name, skill_added', 'required'),
			array('skill_name, skill_img', 'length', 'max'=>50),
			array('skill_desc', 'length', 'max'=>500),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('skill_id, skill_name, skill_desc, skill_img, skill_added', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'jcatQuestions' => array(self::HAS_MANY, 'JcatQuestions', 'skill_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'skill_id' => 'Skill',
			'skill_name' => 'Skill Name',
			'skill_desc' => 'Skill Desc',
			'skill_img' => 'Skill Img',
			'skill_added' => 'Skill Added',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('skill_id',$this->skill_id,true);
		$criteria->compare('skill_name',$this->skill_name,true);
		$criteria->compare('skill_desc',$this->skill_desc,true);
		$criteria->compare('skill_img',$this->skill_img,true);
		$criteria->compare('skill_added',$this->skill_added,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
