<?php

/**
 * This is the model class for table "jcat_badges".
 *
 * The followings are the available columns in table 'jcat_badges':
 * @property integer $id
 * @property integer $user_id
 * @property integer $skill_id
 * @property integer $score
 * @property string $badge_type
 * @property string $processed
 * @property string $date_added
 * @property string $date_modified
 * @property string $badge_image
 */
class Badges extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Badges the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'jcat_badges';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, skill_id, score, badge_type, date_added', 'required'),
			array('user_id, skill_id, score', 'numerical', 'integerOnly'=>true),
			array('badge_type', 'length', 'max'=>20),
			array('processed', 'length', 'max'=>1),
			array('badge_image', 'length', 'max'=>50),
			array('date_modified', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_id, skill_id, score, badge_type, processed, date_added, date_modified, badge_image', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'skill_id' => 'Skill',
			'score' => 'Score',
			'badge_type' => 'Badge Type',
			'processed' => 'Processed',
			'date_added' => 'Date Added',
			'date_modified' => 'Date Modified',
			'badge_image' => 'Badge Image',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('skill_id',$this->skill_id);
		$criteria->compare('score',$this->score);
		$criteria->compare('badge_type',$this->badge_type,true);
		$criteria->compare('processed',$this->processed,true);
		$criteria->compare('date_added',$this->date_added,true);
		$criteria->compare('date_modified',$this->date_modified,true);
		$criteria->compare('badge_image',$this->badge_image,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}