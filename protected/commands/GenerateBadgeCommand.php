<?php

class GenerateBadgeCommand extends CConsoleCommand
{
	public function run()
	{
		$criteria = new CDbCriteria;

		$criteria->condition = "processed='n'";

		$badge_list = Badges::model()->findAll($criteria);
		if(is_array($badge_list)){
		foreach($badge_list as $badge){
			echo $badge->badge_type;
	
			//header('Content-type: image/jpeg');

		// Create Image From Existing File
		$root_path = Yii::app()->params['attachment'];

		$card_path = Yii::app()->params['badge_layoutpath'];
		$badge_upload_path = Yii::app()->params['badgepath'];
		$jpg_image = imagecreatefromjpeg($root_path . $card_path .'/card.jpg');

		// Allocate A Color For The Text
		$blue_color = imagecolorallocate($jpg_image, 153, 216, 231);
		$white = imagecolorallocate($jpg_image, 255, 255, 255);


		// Set Path to Font File
		$font_path = $root_path . $card_path . '/arial.ttf';

		// Set Text to Be Printed On Image
		$skill_name=Skills::model()->findByPk($badge->skill_id)->skill_name;
		$score = $badge->score;
		$rank= "400 of 1000";

		// Print Skill Name On Image
		imagettftext($jpg_image, 40, 0, 40, 100, $white, $font_path, $skill_name);
		// Print score On Image
		imagettftext($jpg_image, 50, 0, 300, 130, $blue_color, $font_path, $score);
		// Print Rank on Image
		imagettftext($jpg_image, 15, 0, 543, 275, $blue_color, $font_path, $rank);
		
		// Create directory to upload the image to new location
		$upload_path = 	$root_path . $badge_upload_path. '/' . $badge->user_id; 
		@mkdir($upload_path,0755,true);
		// Send Image to Browser
		$img_name = $badge->user_id . '_' . $skill_name .'_'. $badge->badge_type . '.jpeg';
		$status = imagejpeg($jpg_image, $upload_path .'/' . $img_name  ,100 );
		if($status){
		echo Badges::model()->updateByPk($badge->id, array('processed'=>'Y', 'badge_image'=>$img_name));
		}

		// Clear Memory
		imagedestroy($jpg_image);

	
	
	
		}
		}
		else{
		echo "No Badge to process";
	}
	}
}

?>    	
