<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Console Application',
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.components.smtp.*',
	),

	// preloading 'log' component
	'preload'=>array('log'),

	// application components
	'components'=>array(
	/*	'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),
		*/
		// uncomment the following to use a MySQL database
		
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=jcat',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => 'abc123',
			'charset' => 'utf8',
		),
		
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),
	),
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
                'attachment'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'../../../', // Path for attachemnt
                'skillspath'=>'jCat/files/skills', // Path for skills
                'badgepath'=>'jCat/files/badges', // Path for skills
                'badge_layoutpath'=>'jCat/assets/badges', // Path for skills
                
	)
);
